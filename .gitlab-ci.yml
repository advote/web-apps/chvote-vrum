#---------------------------------------------------------------------------------------------------
# - #%L                                                                                            -
# - chvote-vrum                                                                                    -
# - %%                                                                                             -
# - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
# - %%                                                                                             -
# - This program is free software: you can redistribute it and/or modify                           -
# - it under the terms of the GNU Affero General Public License as published by                    -
# - the Free Software Foundation, either version 3 of the License, or                              -
# - (at your option) any later version.                                                            -
# -                                                                                                -
# - This program is distributed in the hope that it will be useful,                                -
# - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
# - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
# - GNU General Public License for more details.                                                   -
# -                                                                                                -
# - You should have received a copy of the GNU Affero General Public License                       -
# - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
# - #L%                                                                                            -
#---------------------------------------------------------------------------------------------------

services:
- $IMAGE_DOCKER_DIND

variables:
  DOCKER_DRIVER: overlay
  MAVEN_REPO_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -s $MVN_SETTINGS -Dorg.slf4j.simpleLogger.showDateTime=true"

  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd`are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "-U --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true -Djava.awt.headless=true"

.NPM_CONFIG: &NPM_CONFIG |
  if [ $ORCHESTRATOR == "OCP" ]; then
  npm config -g set registry $NPM_REGISTRY_URL;
  npm config -g set strict-ssl false;
  fi

.NODE_SASS: &NODE_SASS |
  if [ $ORCHESTRATOR == "OCP" ]; then
  mkdir -p chvote-vrum-frontend/node_modules/node-sass;
  cp -R /usr/local/lib/node_modules/node-sass/* chvote-vrum-frontend/node_modules/node-sass/;
  fi

.WEBDRIVER_UPDATE : &WEBDRIVER_UPDATE |
  if [ $ORCHESTRATOR == "OCP" ]; then
    mkdir -p chvote-vrum-frontend/node_modules/webdriver-manager;
    cp -R /usr/local/lib/node_modules/webdriver-manager/* chvote-vrum-frontend/node_modules/webdriver-manager/;
  fi

.DOCKER_LOGIN: &DOCKER_LOGIN |
  if [ $ORCHESTRATOR == "OCP" ]; then
    docker login -u unused -p $(oc whoami -t) docker-registry.default.svc:5000;
  else
    docker login -u $DOCKER_REGISTRY_USER -p $CI_JOB_TOKEN $DOCKER_REGISTRY_URL;
  fi

stages:
- build-artifact
- build-image
- check
- deploy

##################################################
# ARTIFACTS                                      #
##################################################
artifact:frontend:
  before_script:
  - export PATH=$PATH:chvote-vrum-frontend/node_modules/.bin
  - *NPM_CONFIG
  - *NODE_SASS
  image: $IMAGE_DOCKER_NODE_KARMA_PROTRACTOR_CHROME
  stage: build-artifact
  script:
  - cd chvote-vrum-frontend
  - npm install --unsafe-perm
  - npm run build-prod
  - npm run testOnceCI
  artifacts:
    expire_in: 1 week
    paths:
    - chvote-vrum-frontend/dist
    - chvote-vrum-frontend/test_reports/karma
    - chvote-vrum-frontend/test_reports/coverage
    - chvote-vrum-frontend/test_reports/protractor
  cache:
    key: chvote-vrum-frontend-$CI_COMMIT_REF_NAME
    untracked: true
    paths:
    - chvote-vrum-frontend/node_modules/

artifact:backend:
  image: $IMAGE_MVN_JDK11
  stage: build-artifact
  script:
  - cd chvote-vrum-backend
  - if [ $ORCHESTRATOR == "OCP" ]; then
    mvn $MAVEN_REPO_OPTS $MAVEN_CLI_OPTS clean $SONAR_PRE_BUILD_CMD install $SONAR_POST_BUILD_CMD;
    else
    mvn $MAVEN_REPO_OPTS $MAVEN_CLI_OPTS clean $SONAR_PRE_BUILD_CMD install surefire-report:report-only $SONAR_POST_BUILD_CMD;
    fi
  - printf 'BACKEND_ARTIFACT=${project.version}' | mvn $MAVEN_REPO_OPTS help:evaluate | grep '^BACKEND_ARTIFACT' | cut -d = -f 2 > version.txt
  artifacts:
    expire_in: 1 week
    paths:
    - chvote-vrum-backend/chvote-vrum-repository/target/*.jar
    - chvote-vrum-backend/chvote-vrum-repository/target/site
    - chvote-vrum-backend/chvote-vrum-service/target/*.jar
    - chvote-vrum-backend/chvote-vrum-service/target/site
    - chvote-vrum-backend/chvote-vrum-mock-server/chvote-vrum-mock-server-service/target/*.jar
    - chvote-vrum-backend/chvote-vrum-mock-server/chvote-vrum-mock-server-service/target/site
    - chvote-vrum-backend/chvote-vrum-mock-server/chvote-vrum-mock-server-rest/target/*.jar
    - chvote-vrum-backend/chvote-vrum-mock-server/chvote-vrum-mock-server-rest/target/site
    - chvote-vrum-backend/chvote-vrum-rest/target/*.jar
    - chvote-vrum-backend/chvote-vrum-rest/target/site
    - chvote-vrum-backend/version.txt
  cache:
    key: chvote-vrum-backend-$CI_COMMIT_REF_NAME
    paths:
    - .m2/repository/

artifact:design-docs:
  image: $IMAGE_BUILD_DOCS
  stage: build-artifact
  script:
  - cd chvote-vrum-design-docs
  - mvn $MAVEN_REPO_OPTS $MAVEN_CLI_OPTS clean package
  artifacts:
    paths:
    - chvote-vrum-design-docs/target/generated-docs/pdf/vrum-application-design.pdf
  cache:
    key: chvote-vrum-design-docs-$CI_COMMIT_REF_NAME
    paths:
    - .m2/repository/

##################################################
# SONAR                                          #
##################################################
sonar:frontend:
  image: $IMAGE_SONAR_SCANNER
  stage: build-artifact
  before_script:
  - export PATH=$PATH:chvote-vrum-frontend/node_modules/.bin
  - *NPM_CONFIG
  script:
  - cd chvote-vrum-frontend
  - npm install --only=dev typescript
  - sonar-scanner $SONAR_FRONTEND_OPTIONS

##################################################
# IMAGES                                         #
##################################################
image:frontend:
  image: $IMAGE_DOCKER
  stage: build-image
  before_script:
  - *DOCKER_LOGIN
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - cd chvote-vrum-frontend
  - sed -i 's#REGISTRY_BUILD#'"$REGISTRY_BUILD"'#g' Dockerfile
  - docker build -t chvote/chvote-vrum-frontend .
  - docker tag chvote/chvote-vrum-frontend $REGISTRY_APP/chvote-vrum-frontend:$IMAGE_TAG
  - docker push $REGISTRY_APP/chvote-vrum-frontend:$IMAGE_TAG
  dependencies:
  - artifact:frontend

image:backend:
  image: $IMAGE_DOCKER
  stage: build-image
  before_script:
  - *DOCKER_LOGIN
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - cd chvote-vrum-backend
  - export VERSION=$(cat version.txt)
  - cd chvote-vrum-rest
  - sed -i 's#REGISTRY_BUILD#'"$REGISTRY_BUILD"'#g' Dockerfile
  - docker build -t chvote/chvote-vrum-backend --build-arg version=$VERSION .
  - docker tag chvote/chvote-vrum-backend $REGISTRY_APP/chvote-vrum-backend:$IMAGE_TAG
  - docker push $REGISTRY_APP/chvote-vrum-backend:$IMAGE_TAG
  dependencies:
  - artifact:backend

image:mock-server:
  image: $IMAGE_DOCKER
  stage: build-image
  before_script:
  - *DOCKER_LOGIN
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - cd chvote-vrum-backend
  - export VERSION=$(cat version.txt)
  - cd chvote-vrum-mock-server/chvote-vrum-mock-server-rest
  - sed -i 's#REGISTRY_BUILD#'"$REGISTRY_BUILD"'#g' Dockerfile
  - docker build -t chvote/chvote-vrum-mock-server --build-arg version=$VERSION .
  - docker tag chvote/chvote-vrum-mock-server $REGISTRY_APP/chvote-vrum-mock-server:$IMAGE_TAG
  - docker push $REGISTRY_APP/chvote-vrum-mock-server:$IMAGE_TAG
  dependencies:
  - artifact:backend

image:reverse-proxy:
  image: $IMAGE_DOCKER
  stage: build-image
  before_script:
  - *DOCKER_LOGIN
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - cd chvote-vrum-reverse-proxy
  - sed -i 's#REGISTRY_BUILD#'"$REGISTRY_BUILD"'#g' Dockerfile
  - docker build -t chvote/chvote-vrum-reverse-proxy .
  - docker tag chvote/chvote-vrum-reverse-proxy $REGISTRY_APP/chvote-vrum-reverse-proxy:$IMAGE_TAG
  - docker push $REGISTRY_APP/chvote-vrum-reverse-proxy:$IMAGE_TAG


##################################################
# Check                                          #
##################################################
check:create-review-environment:
  image: $IMAGE_DOCKER
  stage: check
  before_script:
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - cd deploy-openshift
  - oc whoami
  # Delete OpenShift project. ( "|| true;" makes sure that the job continues even if the delete does not exit successfully.)
  - oc delete project chvote-review-$CI_COMMIT_REF_SLUG --now=true || true
  - oc new-project chvote-review-$CI_COMMIT_REF_SLUG --description="Review deployment for branch $CI_COMMIT_REF_NAME" --display-name="$CI_COMMIT_REF_NAME"
  # Repeat previous command until return code equals 0
  - while [ $? -ne 0 ]; do !!; done
  # Deploy VRUM application into project
  - oc create -f conf/reverse-proxy-nginx-conf-int_rec.yml
  - oc process -f chvote-vrum-backend.template.yml VRUM_BACKEND_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-mockserver.template.yml VRUM_MOCKSERVER_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-frontend.template.yml VRUM_FRONTEND_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-reverse-proxy.template.yml SUBDOMAIN=$CI_ENVIRONMENT_SLUG | oc create -f -
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_ENVIRONMENT_SLUG.$HOSTNAME_REVIEW/vrum/
    on_stop: check:remove-review-environment
  only:
    variables:
    - $ORCHESTRATOR == "OCP"
  except:
  - master

check:remove-review-environment:
  image: $IMAGE_DOCKER
  stage: check
  when: manual
  variables:
    GIT_STRATEGY: none
  script:
  - oc delete project chvote-review-$CI_COMMIT_REF_SLUG --now=true
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  only:
    variables:
    - $ORCHESTRATOR == "OCP"
  except:
  - master

check:e2e:
  image: $IMAGE_DOCKER_NODE_KARMA_PROTRACTOR_CHROME
  stage: check
  before_script:
  - *DOCKER_LOGIN
  - *NPM_CONFIG
  - *NODE_SASS
  - *WEBDRIVER_UPDATE
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - sed -i 's#REGISTRY_APP#'"$REGISTRY_APP"'#g' chvote-vrum-docker-compose/docker-compose-ci.yml
  - docker-compose -f chvote-vrum-docker-compose/docker-compose-ci.yml up -d
  - docker-compose -f chvote-vrum-docker-compose/docker-compose-ci.yml logs -f > chvote-vrum-docker-compose/docker.log &
  - docker-compose -f chvote-vrum-docker-compose/docker-compose-ci.yml logs -f &
  - cd chvote-vrum-frontend
  - npm install --unsafe-perm
  - if [ $ORCHESTRATOR != "OCP" ]; then
    npm run update-web-driver;
    sleep 2m;
    fi
  # kubernetes executor does not link services correctly
  # see https://gitlab.com/gitlab-org/gitlab-runner/issues/2229
  - if [ $ORCHESTRATOR == "OCP" ]; then
    sed -i 's#docker#'"127.0.0.1"'#g' e2e/protractor.ci.conf.js;
    sed -i 's#http://docker#'"http://127.0.0.1"'#g' package.json;
    fi
  - npm run e2eCI;
  - docker-compose -f ../chvote-vrum-docker-compose/docker-compose-ci.yml down
  artifacts:
    expire_in: 1 week
    when: always
    paths:
    - chvote-vrum-frontend/test_reports/protractor
    - chvote-vrum-docker-compose/docker.log
  dependencies:
  - image:frontend
  - image:backend
  - image:mock-server
  - image:reverse-proxy


##################################################
# DEPLOYMENTS - GCE                              #
##################################################
deploy-to-int:int1:
  image: $IMAGE_GCE_SDK_KUBECTL
  stage: deploy
  before_script:
  - docker login -u $DOCKER_REGISTRY_USER -p $CI_JOB_TOKEN $DOCKER_REGISTRY_URL;
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - cd deploy
  - echo "$GOOGLE_KEY" > key.json
  - gcloud auth activate-service-account --key-file key.json
  - gcloud config set compute/zone $K8S_COMPUTE_ZONE
  - gcloud config set project $K8S_PROJECT
  - gcloud config set container/use_client_certificate True
  - gcloud container clusters get-credentials $GCE_CLUSTER_INT1
  - kubectl delete secret registry.gitlab.com --ignore-not-found
  - kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=$REGISTRY_USER --docker-password=$REGISTRY_PASSWD --docker-email=$REGISTRY_EMAIL
  - sed -i 's#SPRING_PROFILES_ACTIVE_VALUE#int#g' backend-deployment.yml
  - sed -i 's#SPRING_PROFILES_ACTIVE_VALUE#int#g' mock-server-deployment.yml
  - sed -i "s#IMAGE_TAG#$IMAGE_TAG#g" *-deployment.yml
  - kubectl apply -f reverse-proxy-deployment.yml -f reverse-proxy-service.yml -f backend-deployment.yml -f backend-service.yml -f frontend-deployment.yml -f frontend-service.yml -f mock-server-deployment.yml -f mock-server-service.yml
  - kubectl scale --replicas=0 deployment chvote-vrum
  - kubectl scale --replicas=1 deployment chvote-vrum
  - kubectl scale --replicas=0 deployment chvote-vrum-backend
  - kubectl scale --replicas=1 deployment chvote-vrum-backend
  - kubectl scale --replicas=0 deployment chvote-vrum-frontend
  - kubectl scale --replicas=1 deployment chvote-vrum-frontend
  - kubectl scale --replicas=0 deployment chvote-vrum-mock-server
  - kubectl scale --replicas=1 deployment chvote-vrum-mock-server
  environment:
    name: int1
    url: http://$HOSTNAME_INT1/chvote-vrum
  only:
    refs:
    - master
    variables:
    - $ORCHESTRATOR == "GCE"


deploy-to-rec:rec1:
  before_script:
  - docker login -u $DOCKER_REGISTRY_USER -p $CI_JOB_TOKEN $DOCKER_REGISTRY_URL;
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  image: $IMAGE_GCE_SDK_KUBECTL
  stage: deploy
  script:
  - cd deploy
  - echo "$GOOGLE_KEY" > key.json
  - gcloud auth activate-service-account --key-file key.json
  - gcloud config set compute/zone $K8S_COMPUTE_ZONE
  - gcloud config set project $K8S_PROJECT
  - gcloud config set container/use_client_certificate True
  - gcloud container clusters get-credentials $GCE_CLUSTER_REC1
  - kubectl delete secret registry.gitlab.com --ignore-not-found
  - kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=$REGISTRY_USER --docker-password=$REGISTRY_PASSWD --docker-email=$REGISTRY_EMAIL
  - sed -i 's#SPRING_PROFILES_ACTIVE_VALUE#rec#g' backend-deployment.yml
  - sed -i 's#SPRING_PROFILES_ACTIVE_VALUE#rec#g' mock-server-deployment.yml
  - sed -i "s#IMAGE_TAG#$IMAGE_TAG#g" *-deployment.yml
  - kubectl apply -f reverse-proxy-deployment.yml -f reverse-proxy-service.yml -f backend-deployment.yml -f backend-service.yml -f frontend-deployment.yml -f frontend-service.yml -f mock-server-deployment.yml -f mock-server-service.yml
  - kubectl scale --replicas=0 deployment chvote-vrum
  - kubectl scale --replicas=1 deployment chvote-vrum
  - kubectl scale --replicas=0 deployment chvote-vrum-backend
  - kubectl scale --replicas=1 deployment chvote-vrum-backend
  - kubectl scale --replicas=0 deployment chvote-vrum-frontend
  - kubectl scale --replicas=1 deployment chvote-vrum-frontend
  - kubectl scale --replicas=0 deployment chvote-vrum-mock-server
  - kubectl scale --replicas=1 deployment chvote-vrum-mock-server
  environment:
    name: rec1
    url: http://$HOSTNAME_REC1/chvote-vrum
  only:
    refs:
    - master
    variables:
    - $ORCHESTRATOR == "GCE"
  when: manual

deploy-to-rec:rec2:
  before_script:
  - docker login -u $DOCKER_REGISTRY_USER -p $CI_JOB_TOKEN $DOCKER_REGISTRY_URL;
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  image: $IMAGE_GCE_SDK_KUBECTL
  stage: deploy
  script:
  - cd deploy
  - echo "$GOOGLE_KEY" > key.json
  - gcloud auth activate-service-account --key-file key.json
  - gcloud config set compute/zone $K8S_COMPUTE_ZONE
  - gcloud config set project $K8S_PROJECT
  - gcloud config set container/use_client_certificate True
  - gcloud container clusters get-credentials $GCE_CLUSTER_REC2
  - kubectl delete secret registry.gitlab.com --ignore-not-found
  - kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=$REGISTRY_USER --docker-password=$REGISTRY_PASSWD --docker-email=$REGISTRY_EMAIL
  - sed -i 's#SPRING_PROFILES_ACTIVE_VALUE#rec#g' backend-deployment.yml
  - sed -i 's#SPRING_PROFILES_ACTIVE_VALUE#rec#g' mock-server-deployment.yml
  - sed -i "s#IMAGE_TAG#$IMAGE_TAG#g" *-deployment.yml
  - kubectl apply -f reverse-proxy-deployment.yml -f reverse-proxy-service.yml -f backend-deployment.yml -f backend-service.yml -f frontend-deployment.yml -f frontend-service.yml -f mock-server-deployment.yml -f mock-server-service.yml
  - kubectl scale --replicas=0 deployment chvote-vrum
  - kubectl scale --replicas=1 deployment chvote-vrum
  - kubectl scale --replicas=0 deployment chvote-vrum-backend
  - kubectl scale --replicas=1 deployment chvote-vrum-backend
  - kubectl scale --replicas=0 deployment chvote-vrum-frontend
  - kubectl scale --replicas=1 deployment chvote-vrum-frontend
  - kubectl scale --replicas=0 deployment chvote-vrum-mock-server
  - kubectl scale --replicas=1 deployment chvote-vrum-mock-server
  environment:
    name: rec2
    url: http://$HOSTNAME_REC2/chvote-vrum
  only:
    refs:
    - master
    variables:
    - $ORCHESTRATOR == "GCE"
  when: manual

##################################################
# DEPLOYMENTS INT/UAT - OCP                      #
##################################################
deploy-to-int:OCP-int1:
  image: $IMAGE_DOCKER
  stage: deploy
  before_script:
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - cd deploy-openshift
  - oc whoami
  - oc project chvote-int1
  # Removes VRUM application from project
  - oc delete all --selector app=VRUM
  - oc delete sa --selector app=VRUM
  - oc delete cm --selector app=VRUM
  # Deploy VRUM application into project
  - oc create -f conf/reverse-proxy-nginx-conf-int_rec.yml
  - oc process -f chvote-vrum-backend.json VRUM_BACKEND_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-mockserver.json VRUM_MOCKSERVER_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-frontend.json VRUM_FRONTEND_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-reverse-proxy.json SUBDOMAIN=int1 | oc create -f -
  environment:
    name: int1
    url: https://$HOSTNAME_INT1/vrum/
  only:
    refs:
    - master
    variables:
    - $ORCHESTRATOR == "OCP"

deploy-to-uat:OCP-rec-1:
  image: $IMAGE_DOCKER
  stage: deploy
  when: manual
  before_script:
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - cd deploy-openshift
  - oc whoami
  - oc project chvote-rec1
  # Removes VRUM application from project
  - oc delete all --selector app=VRUM
  - oc delete sa --selector app=VRUM
  - oc delete cm --selector app=VRUM
  # Deploy VRUM application into project
  - oc create -f conf/reverse-proxy-nginx-conf-int_rec.yml
  - oc process -f chvote-vrum-backend.json VRUM_BACKEND_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-mockserver.json VRUM_MOCKSERVER_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-frontend.json VRUM_FRONTEND_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-reverse-proxy.json SUBDOMAIN=rec1 | oc create -f -
  environment:
    name: rec/1
    url: https://$HOSTNAME_REC1/vrum/
  only:
    refs:
    - master
    variables:
    - $ORCHESTRATOR == "OCP"

deploy-to-uat:OCP-rec-2:
  image: $IMAGE_DOCKER
  stage: deploy
  when: manual
  before_script:
  - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  script:
  - cd deploy-openshift
  - oc whoami
  - oc project chvote-rec2
  # Removes VRUM application from project
  - oc delete all --selector app=VRUM
  - oc delete sa --selector app=VRUM
  - oc delete cm --selector app=VRUM
  # Deploy VRUM application into project
  - oc create -f conf/reverse-proxy-nginx-conf-int_rec.yml
  - oc process -f chvote-vrum-backend.json VRUM_BACKEND_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-mockserver.json VRUM_MOCKSERVER_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-frontend.json VRUM_FRONTEND_IMAGE_TAG=$IMAGE_TAG | oc create -f -
  - oc process -f chvote-vrum-reverse-proxy.json SUBDOMAIN=rec2 | oc create -f -
  environment:
    name: rec/2
    url: https://$HOSTNAME_REC2/vrum/
  only:
    refs:
    - master
    variables:
    - $ORCHESTRATOR == "OCP"