/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, ParamMap, RouterModule, Routes } from '@angular/router';
import { AuthorizationGuard } from '../core/authorization/authorization.guard';
import { OperationDetailResolver } from '../core/operations/operation-detail.resolver';
import { OperationsListResolver } from '../core/operations/operations-list.resolver';

import { CardProcessingComponent } from '../voting-cards/card-processing/card-processing.component';
import { RegistryImportComponent } from '../voting-cards/registry-import/registry-import.component';
import { SearchComponent } from '../voting-cards/search/search.component';
import { OperationDetailComponent } from './operation-detail/operation-detail.component';

import { OperationsListComponent } from './operations-list/operations-list.component';

const redirectTo = (route: ActivatedRouteSnapshot, module: string) => {
  return `/operations/${route.parent.params['id']}/${module}`;
};

const redirectToHome = (route: ActivatedRouteSnapshot) => '/operations';
const redirectToSearch = (route: ActivatedRouteSnapshot) => redirectTo(route, 'search');
const redirectToRegistryImport = (route: ActivatedRouteSnapshot) => redirectTo(route, 'regitry-import');

const routes: Routes = [
  {
    path: 'operations', component: OperationsListComponent,
    resolve: {operations: OperationsListResolver},
    canActivate: [AuthorizationGuard],
    data: {role: 'ROLE_SELECT_OPERATION'}
  },
  {
    path: 'operations/:id', component: OperationDetailComponent,
    resolve: {operation: OperationDetailResolver},
    children: [
      {
        path: 'card-processing',
        component: CardProcessingComponent,
        canActivate: [AuthorizationGuard],
        data: {
          role: 'ROLE_ACCESS_CARD_PROCESSING',
          redirectTo: redirectToSearch
        }
      },
      {
        path: 'search',
        component: SearchComponent,
        canActivate: [AuthorizationGuard],
        data: {
          role: 'ROLE_ACCESS_SEARCH',
          redirectTo: redirectToRegistryImport
        }
      },
      {
        path: 'registry-import',
        component: RegistryImportComponent,
        canActivate: [AuthorizationGuard],
        data: {
          role: 'ROLE_ACCESS_REGISTRY_IMPORT',
          redirectTo: redirectToHome
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class OperationsRoutingModule {
}
