/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpClient } from '@angular/common/http';

import { BaseUrlService } from '../base-url/base-url.service';
import { OperationsService } from './operations.service';

describe('OperationsService', () => {
  const baseUrlServiceMock: BaseUrlService = <any> {
    backendBaseUrl: '/api'
  };

  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let operationService: OperationsService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    operationService = new OperationsService(<any> httpClientSpy, baseUrlServiceMock);
  });

  it('should be created', () => {
    expect(operationService).toBeTruthy();
  });

  it('#getOperations should return an array of operations', () => {
    operationService.getOperations();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/api/operations?test=false');
  });

  it('#getOperation should return an operation', () => {
    operationService.getOperation(1);

    expect(httpClientSpy.get).toHaveBeenCalledWith('/api/operations/1');
  });
});
