/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AUTHORIZATION_KEY } from './authentication.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  constructor(private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const reqWithAuth = this.addAuthentication(req);

    return next.handle(reqWithAuth).pipe(
      tap(
        (event: HttpEvent<any>) => {
        }, // nothing specific to do on success here
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) { // authentication failed
              this.router.navigate(['login']);
            }
          }
        }
      )
    );
  }

  /**
   * This is only available in not provided mode.
   * In production authentication should be provided by the "container"
   * @param {HttpRequest<any>} req
   * @returns {HttpRequest<any>}
   */
  private addAuthentication(req: HttpRequest<any>) {
    const authorization = localStorage.getItem(AUTHORIZATION_KEY);

    let headers = (req.headers || new HttpHeaders()).append('X-Requested-With', 'XMLHttpRequest');
    if (authorization) {
      headers = headers.append('Authorization', `Basic ${authorization}`);
    }

    return req.clone({headers: headers});
  }
}
