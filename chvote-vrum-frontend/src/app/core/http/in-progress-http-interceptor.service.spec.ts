/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpEvent, HttpHandler, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

import { Subject } from 'rxjs';

import { BACKGROUND_PROCESSING, InProgressHttpInterceptor } from './in-progress-http-interceptor.service';
import { LoadingSpinnerService } from './loading-spinner.service';

describe('InProgressHttpInterceptor', () => {

  let loadingSpinnerServiceSpy;
  let interceptor;
  let next: HttpHandler;
  const responseReceived = new Subject<HttpEvent<any>>();

  beforeEach(() => {
    loadingSpinnerServiceSpy = jasmine.createSpyObj('LoadingSpinnerService',
      ['startLoading', 'endLoading']);
    interceptor = new InProgressHttpInterceptor(loadingSpinnerServiceSpy);
    next = <any>{handle: () => responseReceived};
  });

  it('should display a spinner if it\'s not a background process', () => {
    responseReceived.next(new HttpResponse({body: 'test'}));
    const req = new HttpRequest('GET', 'url', null);
    interceptor.intercept(req, next).subscribe(() => {});

    expect(loadingSpinnerServiceSpy.startLoading).toHaveBeenCalled();
    expect(loadingSpinnerServiceSpy.endLoading).not.toHaveBeenCalled();
    responseReceived.next(<HttpEvent<any>>{});
    responseReceived.complete(); // For finally ...
    expect(loadingSpinnerServiceSpy.endLoading).toHaveBeenCalled();
  });

  it('shouldn\'t display a spinner if it\'s a background process', () => {
    const req = new HttpRequest(
      'GET',
      'url', null,
      {headers: new HttpHeaders().append(BACKGROUND_PROCESSING, 'true')}
    );
    interceptor.intercept(req, next).subscribe(() => {});

    expect(loadingSpinnerServiceSpy.startLoading).not.toHaveBeenCalled();
    responseReceived.next(null);
    responseReceived.complete(); // For finally ...
    expect(loadingSpinnerServiceSpy.endLoading).not.toHaveBeenCalled();
  });

});
