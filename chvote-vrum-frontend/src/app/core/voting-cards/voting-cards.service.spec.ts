/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import { BaseUrlService } from '../base-url/base-url.service';
import { VotintCardIdentifierType } from '../configuration/configuration.service';
import { VotingCard, VotingChannel, VotingRightStatus } from './voting-card.model';
import { VotingCardsService } from './voting-cards.service';

describe('VotingCardsService', () => {

  const baseUrlServiceMock: BaseUrlService = <any> {
    backendBaseUrl: '/api'
  };

  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let votingCardService: VotingCardsService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch']);
    votingCardService = new VotingCardsService(<any> httpClientSpy, baseUrlServiceMock);
  });

  it('should be created', () => {
    expect(votingCardService).toBeTruthy();
  });

  it('#searchByVoterIndex should return a voting card', () => {
    const expectedResponse = {
      status: 200,
      body: createVotingCard()
    };
    const operationIdStub = 1;
    const voterIndex = 1;

    httpClientSpy.get.and.returnValue(of(expectedResponse));

    votingCardService.searchByVoterIndex(operationIdStub, voterIndex)
      .toPromise()
      .then(response => {
        expect(response.status).toBe(expectedResponse.status);
        expect(response.body).toBe(expectedResponse.body);
      });
  });

  it('#markAsUsed should register the use of a voting card and return a 204 HTTP status', () => {
    const expectedResponse = {status: 204};

    const operationIdStub = 1;
    const voterIndexStub = 1;
    const votingChannelStub = VotingChannel.MAIL;

    httpClientSpy.patch.and.returnValue(of(expectedResponse));

    votingCardService.markAsUsed(
      operationIdStub, voterIndexStub, votingChannelStub
    )
      .toPromise()
      .then(response => {
        expect(response.status).toBe(expectedResponse.status);
      });
  });

  it('#markAsBlocked should block a voting card and return a 204 HTTP status', () => {
    const expectedResponse = {status: 204};

    const operationIdStub = 1;
    const voterIndexStub = 1;

    httpClientSpy.patch.and.returnValue(of(expectedResponse));

    votingCardService.markAsBlocked(operationIdStub, voterIndexStub)
      .toPromise()
      .then(response => {
        expect(response.status).toBe(expectedResponse.status);
      });
  });

  it('#searchByIdentifier should return a list of voting cards', () => {
    const expectedVotingCards = <VotingCard[]>[
      createVotingCard(),
      createVotingCard(2, 2),
      createVotingCard(3, 3)
    ];

    httpClientSpy.get.and.returnValue(of(expectedVotingCards));

    votingCardService
      .searchByIdentifier(1, VotintCardIdentifierType.LOCAL_PERSON_ID, 'LOCAL_P_1')
      .subscribe(response => {
        expect(response).toBe(expectedVotingCards);
      });
  });

  function createVotingCard(id: number = 1, voterIndex: number = 1) {
    return <VotingCard>{
      id: id,
      operationId: 1,
      voterIndex: voterIndex,
      identificationCodeHash: 'ID_HASH_1',
      localPersonId: 'LOCAL_P_1',
      voterBirthDate: {
        voterBirthYear: 1970,
        voterBirthMonth: 10,
        voterBirthDay: 9
      },
      votingDatetime: new Date(),
      votingRightStatus: VotingRightStatus.AVAILABLE,
      votingChannel: VotingChannel.E_VOTING,
      countingCircle: {
        businessId: '6621',
        name: 'Genève'
      },
      domainOfInfluences: [{
        businessId: 'GE',
        name: 'Genève'
      }]
    };
  }

});
