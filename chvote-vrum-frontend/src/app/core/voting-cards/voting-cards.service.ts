/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseUrlService } from '../base-url/base-url.service';
import { VotintCardIdentifierType } from '../configuration/configuration.service';
import { VotingCard, VotingChannel, VotingRightStatus } from './voting-card.model';

@Injectable({
  providedIn: 'root'
})
export class VotingCardsService {

  constructor(private http: HttpClient, private baseUrlService: BaseUrlService) {
  }

  searchByVoterIndex(operationId: number, voterIndex: number): Observable<HttpResponse<VotingCard>> {
    return this.http.get<VotingCard>(
      this.getServiceUrl(operationId, voterIndex), {observe: 'response'}
    );
  }

  markAsUsed(operationId: number,
             voterIndex: number,
             votingChannel: VotingChannel): Observable<HttpResponse<string>> {
    const serviceUrl = this.getServiceUrl(operationId, voterIndex);
    const headers = this.getPutHeaders();
    const params = new HttpParams()
      .set('status', VotingRightStatus.USED)
      .set('votingChannel', votingChannel);

    return this.http.patch<string>(serviceUrl, params.toString(), {headers, observe: 'response'});
  }

  markAsBlocked(operationId: number,
                voterIndex: number): Observable<HttpResponse<string>> {
    const serviceUrl = this.getServiceUrl(operationId, voterIndex);
    const headers = this.getPutHeaders();
    const params = new HttpParams()
      .set('status', VotingRightStatus.BLOCKED);

    return this.http.patch<string>(serviceUrl, params.toString(), {headers, observe: 'response'});
  }

  searchByIdentifier(operationId: number,
                     identifierName: string,
                     identifier: string): Observable<VotingCard[]> {
    const params = new HttpParams()
      .set(identifierName, identifier);

    return this.http.get<VotingCard[]>(
      `${this.baseUrlService.backendBaseUrl}/operations/${operationId}/voting-cards?${params.toString()}`
    );
  }

  private getServiceUrl(operationId: number, voterIndex: number) {
    return `${this.baseUrlService.backendBaseUrl}/operations/${operationId}/voting-cards/${voterIndex}`;
  }

  private getPutHeaders(): HttpHeaders {
    return new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
  }

}
