/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, RouterModule } from '@angular/router';

import { of } from 'rxjs';
import { AuthorizationService } from '../../core/authorization/authorization.service';
import { User } from '../../core/authorization/user.model';

import { BaseUrlService } from '../../core/base-url/base-url.service';
import { CoreModule } from '../../core/core.module';
import { MaterialModule } from '../../material/material.module';
import { SharedModule } from '../../shared/shared.module';

import { CardProcessingComponent } from './card-processing.component';

class MockActivatedRoute {
  parent: any;
  snapshot = {
    params: {}
  };

  constructor(options) {
    this.parent = options.parent;
    this.snapshot.params = options.params;
  }
}

describe('CardProcessingComponent', () => {
  let component: CardProcessingComponent;
  let fixture: ComponentFixture<CardProcessingComponent>;
  let votingCardsServiceSpy;
  let authorizationServiceSpy;
  const activatedRouteMock = <any> new MockActivatedRoute({
    parent: new MockActivatedRoute({
      params: of({id: '1'})
    })
  });
  const baseUrlServiceMock = jasmine.createSpy('BaseUrlService');

  beforeEach(async(() => {
    votingCardsServiceSpy = jasmine.createSpyObj('VotingCardsService', ['searchByVoterIndex']);
    authorizationServiceSpy = jasmine.createSpyObj('AuthorizationService',
      ['hasAtLeastOneRole']);

    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        CoreModule,
        FlexLayoutModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([]),
        SharedModule
      ],
      providers: [
        CardProcessingComponent,
        {provide: ActivatedRoute, useValue: activatedRouteMock},
        {provide: BaseUrlService, useValue: baseUrlServiceMock},
        {provide: APP_BASE_HREF, useValue: '/'},
        {provide: AuthorizationService, useValue: authorizationServiceSpy}
      ],
      declarations: [CardProcessingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('selecting a channel should change the selected channel', () => {
    authorizationServiceSpy.hasAtLeastOneRole.and.returnValue(true);

    const channelStub = 'blocking';
    component.onChannelSelected(channelStub);

    expect(component.selectedVotingChannel).toBe(channelStub);
  });

});
