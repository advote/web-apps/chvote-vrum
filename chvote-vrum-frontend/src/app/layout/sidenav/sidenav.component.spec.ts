/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { APP_BASE_HREF } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { of } from 'rxjs';

import { AuthorizationService } from '../../core/authorization/authorization.service';
import { User } from '../../core/authorization/user.model';
import { BaseUrlService } from '../../core/base-url/base-url.service';
import { CoreModule } from '../../core/core.module';
import { Version } from '../../core/version/version.model';
import { VersionService } from '../../core/version/version.service';
import { MaterialModule } from '../../material/material.module';
import { SidenavComponent } from './sidenav.component';

describe('SidenavComponent', () => {
  let component: SidenavComponent;
  let fixture: ComponentFixture<SidenavComponent>;

  const baseUrlServiceMock = jasmine.createSpy('BaseUrlService');
  let versionServiceSpy;

  beforeEach(async(() => {
    versionServiceSpy = jasmine.createSpyObj('VersionService', ['get']);

    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        CoreModule,
        NoopAnimationsModule,
        FlexLayoutModule,
        RouterModule.forRoot([])
      ],
      providers: [
        SidenavComponent,
        {provide: BaseUrlService, useValue: baseUrlServiceMock},
        {provide: VersionService, useValue: versionServiceSpy},
        {provide: APP_BASE_HREF, useValue: '/'}
      ],
      declarations: [SidenavComponent]
    }).compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(SidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create the sidenav', async(() => {
    // const sidenav = fixture.debugElement.componentInstance;
    expect(component).toBeDefined();
  }));

  it('should update version', async(() => {
    const v: Version = {buildNumber: '1.0.0', buildTimestamp: new Date()};
    versionServiceSpy.get.and.returnValue(of(v));

    component.ngOnInit();
    const authServ = TestBed.get(AuthorizationService);
    authServ.userChange.next(<User> {login: 'login'});

    expect(component.version).toBe(v);
  }));
});
