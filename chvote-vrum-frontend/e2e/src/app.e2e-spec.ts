/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { escapeRegExp } from 'tslint/lib/utils';
import { CardProcessingController } from './controller/card-processing.po';
import { LoginController } from './controller/login.po';
import { OperationDetailController } from './controller/operation-detail.po';
import { OperationsListController } from './controller/operations-list.po';
import { RegistryImportController } from './controller/registry-import.po';
import { SearchController } from './controller/search.po';
import { VRUMController } from './controller/vrum.po';
import { LoginTestHelper } from './helper/LoginTestHelper';
import { MockServer } from './helper/mock-server';
import { browser, by, element } from 'protractor';

describe('VRUM e2e', () => {

  describe('navigation', () => {
    beforeAll((done) => {
      browser.controlFlow().execute(
        () => MockServer.resetDB()
          .then(() => MockServer.createMockData())
          .then(() => VRUMController.open())
          .then(done)
      );
    });

    it('should display title', () => {
      expect(VRUMController.getTitle()).toEqual('CHVote VRUM');
    });

    it('should display login form', () => {
      expect(LoginController.isLoginPage).toBeTruthy();
    });

    it('should fail to login with wrong creds', () => {
      LoginController.login('toto', 'not a password');
      expect(LoginController.isLoginPage).toBeTruthy();
    });

    LoginTestHelper.testValidLogin('ge1', 'pass');

    it('should navigate to the operations list page', () => {
      OperationsListController.open().then(() => {
        expect(OperationsListController.isOperationsListPage).toBeTruthy();
        expect(OperationsListController.numberOfRows).toBe(4);
      });
    });

    it('should navigate to an operation detail page', () => {
      OperationDetailController.open().then(() => {
        expect(OperationDetailController.isOperationDetailPage).toBeTruthy();
        expect(OperationDetailController.navigationLinks.count()).toBe(3);
      });
    });

    it('should navigate to card processing page', () => {
      CardProcessingController.open()
        .then(() => {
          expect(CardProcessingController.isCardProcessingPage).toBeTruthy();
          expect(CardProcessingController.isButtonMailChannelPresent).toBeTruthy();
          expect(CardProcessingController.isButtonPollingChannelPresent).toBeTruthy();
          expect(CardProcessingController.isButtonSveChannelPresent).toBeTruthy();
          expect(CardProcessingController.isButtonBlockingChannelPresent).toBeTruthy();
        });
    });

    it('selecting a voting channel should display the search section', () => {
      CardProcessingController.buttonMailChannelClick.then(() => {
        expect(CardProcessingController.idCardSearchPresent).toBeTruthy();
      });

      CardProcessingController.buttonPollingChannelClick.then(() => {
        expect(CardProcessingController.idCardSearchPresent).toBeTruthy();
      });

      CardProcessingController.buttonSveChannelClick.then(() => {
        expect(CardProcessingController.idCardSearchPresent).toBeTruthy();
      });

      CardProcessingController.buttonBlockingChannelClick.then(() => {
        expect(CardProcessingController.idCardSearchPresent).toBeTruthy();
      });
    });

    it('searching for a voting card with an unknown identifier should display an error message', () => {
      CardProcessingController.buttonMailChannelClick.then(() => {
        CardProcessingController.inputIdentifierAndSubmitForm(5000)
          .then(() => {
            expect(CardProcessingController.isVotingCardPresent).toBeTruthy();
            expect(CardProcessingController.votingCardStatus).toBe('Identifiant inconnu');
          });
      });
    });

    it('searching a voting card "AVAILABLE" should display the voting card with a message indicating that no vote has been registered',
      () => {
      CardProcessingController.buttonMailChannelClick.then(() => {
        CardProcessingController.inputIdentifierAndSubmitForm(1)
          .then(() => {
            expect(CardProcessingController.isVotingCardPresent).toBeTruthy();
            expect(CardProcessingController.votingCardStatus).toBe('Pas de vote enregistré');
          });
      });
    });

    it('saving the use of a voting right should display a confirmation message', () => {
        CardProcessingController.focusInputAndSubmitForm()
          .then(() => {
            expect(CardProcessingController.isVotingCardPresent).toBeTruthy();
            expect(CardProcessingController.votingCardStatus)
              .toMatch('Vote enregistré le (([0-9]{2}\/){2}[0-9]{4}) par le canal [a-zA-Z]+');
          });
    });

    it('blocking a voting card should display a confirmation message', () => {
      CardProcessingController.buttonBlockingChannelClick.then(() => {
        CardProcessingController.inputIdentifierAndSubmitForm(5)
          .then(() => {
              expect(CardProcessingController.votingCardStatus)
                .toMatch('Carte bloquée');
          });
      });
    });

    it('trying to block a voting card already blocked should display an error message', () => {
      CardProcessingController.focusInputAndSubmitForm()
        .then(() => {
          expect(CardProcessingController.votingCardStatus)
            .toMatch('Carte déjà bloquée');
        });
    });

    it('should navigate to search page', () => {
      SearchController.open()
        .then(() => {
          expect(SearchController.isSearchPage).toBeTruthy();
        });
    });

    it('searching voting cards with existing identifier should display a list of the corresponding voting cards', () => {
      SearchController.inputIdentifierAndSubmitForm('0000')
        .then(() => {
          expect(SearchController.isSearchResultsPresent).toBeTruthy();
          expect(SearchController.numberOfCardRows).toBe(1);
        });
    });

    it('searching voting cards with non-existing identifier should display an error message', () => {
      SearchController.inputIdentifierAndSubmitForm('00000000001')
        .then(() => {
          expect(SearchController.isSearchResultsPresent).toBeTruthy();
          expect(SearchController.searchResultsErrorText)
            .toMatch('ID électeur inconnu');
        });
    });

    it('should navigate to registry import page', () => {
      RegistryImportController.open()
        .then(() => {
          expect(RegistryImportController.isRegistryImportPage).toBeTruthy();
        });
    });

    LoginTestHelper.testLogout();
    LoginTestHelper.testValidLogin('ge3', 'pass');

    it('should redirect to search if the user does not have the roles to see the card processing page', () => {
      OperationDetailController.open().then(() => {
        expect(SearchController.isSearchPage).toBeTruthy();
      });
    });
  });

});
