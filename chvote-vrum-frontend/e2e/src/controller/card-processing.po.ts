/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions, Key } from 'protractor';
import { OperationDetailController } from './operation-detail.po';

export class CardProcessingController {

  private static get cardProcessing() {
    return element(by.css('.card-processing-section'));
  }

  private static get cardSearch() {
    return element(by.css('.card-search-section'));
  }

  private static get votingCard() {
    return element(by.css('.voting-card-section'));
  }

  private static get buttonMailChannel() {
    return element(by.css('#btn-channel-MAIL'));
  }

  private static get buttonPollingChannel() {
    return element(by.css('#btn-channel-POLLING_STATION'));
  }

  private static get buttonSveChannel() {
    return element(by.css('#btn-channel-OTHER'));
  }

  private static get buttonBlockingChannel() {
    return element(by.css('#btn-channel-BLOCKING'));
  }

  private static get status() {
    return element(by.css('.voting-card-section .status'));
  }

  static get isButtonMailChannelPresent() {
    return this.buttonMailChannel.isPresent();
  }

  static get isButtonPollingChannelPresent() {
    return this.buttonPollingChannel.isPresent();
  }

  static get isButtonSveChannelPresent() {
    return this.buttonSveChannel.isPresent();
  }

  static get isButtonBlockingChannelPresent() {
    return this.buttonBlockingChannel.isPresent();
  }

  static get buttonMailChannelClick() {
    return this.buttonMailChannel.click();
  }

  static get buttonPollingChannelClick() {
    return this.buttonPollingChannel.click();
  }

  static get buttonSveChannelClick() {
    return this.buttonSveChannel.click();
  }

  static get buttonBlockingChannelClick() {
    return this.buttonBlockingChannel.click();
  }

  static get isCardProcessingPage() {
    return this.cardProcessing.isPresent();
  }

  static get idCardSearchPresent() {
    return this.cardSearch.isPresent();
  }

  static get isVotingCardPresent() {
    return this.votingCard.isPresent();
  }

  static get votingCardStatus() {
    return this.status.getText();
  }

  static inputIdentifierAndSubmitForm(voterIdStub: number) {
    return element(by.css('#card-identifier')).sendKeys(voterIdStub)
      .then(() => {
        browser.actions().sendKeys(Key.ENTER).perform()
          .then(() => browser.wait(ExpectedConditions.presenceOf(this.votingCard)));
      });
  }

  static focusInputAndSubmitForm() {
    return element(by.css('#card-identifier')).click()
      .then(() => {
        browser.actions().sendKeys(Key.ENTER).perform()
          .then(() => browser.wait(ExpectedConditions.presenceOf(this.votingCard)));
      });
  }

  static open() {
    return OperationDetailController.tabCardProcessing.click()
      .then(() => browser.wait(ExpectedConditions.presenceOf(this.cardProcessing)));
  }

}
