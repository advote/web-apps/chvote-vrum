/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions } from 'protractor';

export class LoginController {

  private static get loginForm() {
    return element(by.id('loginForm'));
  }

  private static get logoutBtn() {
    return element(by.id('logoutButton'));
  }

  private static get usernameField() {
    return element(by.css('input[name=username]'));
  }

  private static get passwordField() {
    return element(by.css('input[name=password]'));
  }

  private static get submitButton() {
    return element(by.css('button[type=submit]'));
  }

  static get isLoginPage() {
    return LoginController.loginForm.isPresent();
  }

  static openLoginMenu() {
    return element(by.id('vrum-login-menu-btn')).click()
      .then(() => browser.wait(ExpectedConditions.elementToBeClickable(LoginController.logoutBtn)));
  }

  static closeLoginMenu() {
    return element(by.tagName('body')).click();
  }

  static get currentUser() {
    return LoginController.openLoginMenu()
      .then(() => element(by.id('vrum-current-user')).getText())
      .then(text => LoginController.closeLoginMenu().then(() => text));
  }

  static login(username: string, password: string) {
    LoginController.usernameField.clear();
    LoginController.passwordField.clear();
    LoginController.usernameField.sendKeys(username);
    LoginController.passwordField.sendKeys(password);
    LoginController.submitButton.click();
  }

  static logout() {
    return LoginController.openLoginMenu()
      .then(() => browser.executeScript('document.getElementById("logoutButton").click();'))
      .then(() => browser.wait(ExpectedConditions.presenceOf(LoginController.loginForm)));
  }

}
