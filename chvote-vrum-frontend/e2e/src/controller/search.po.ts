/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions, Key } from 'protractor';
import { OperationDetailController } from './operation-detail.po';

export class SearchController {

  private static get search() {
    return element(by.css('.search-section'));
  }

  private static get searchResults() {
    return element(by.css('.search-results'));
  }

  private static get inputCardIdentifier() {
    return element(by.css('#card-identifier'));
  }

  /**
   * Retrieve voting card results.
   */
  private static get votingCardRows() {
    return element.all(by.css('.voting-card-container'));
  }

  static get isSearchPage() {
    return this.search.isPresent();
  }

  static get isSearchResultsPresent() {
    return this.searchResults.isPresent();
  }

  static get numberOfCardRows() {
    return this.votingCardRows.count();
  }

  static get searchResultsErrorText() {
    return this.searchResults.getText();
  }

  static open() {
    return OperationDetailController.tabSearch.click()
      .then(() => browser.wait(ExpectedConditions.presenceOf(this.search)));
  }

  static inputIdentifierAndSubmitForm(voterIdentifierStub: string) {
    return this.inputCardIdentifier.sendKeys(voterIdentifierStub)
      .then(() => {
        browser.actions().sendKeys(Key.ENTER).perform()
          .then(() => {
            browser.wait(ExpectedConditions.presenceOf(this.searchResults));
          });
      });
  }

}
