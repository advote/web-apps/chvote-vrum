#---------------------------------------------------------------------------------------------------
# - #%L                                                                                            -
# - chvote-vrum                                                                                    -
# - %%                                                                                             -
# - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
# - %%                                                                                             -
# - This program is free software: you can redistribute it and/or modify                           -
# - it under the terms of the GNU Affero General Public License as published by                    -
# - the Free Software Foundation, either version 3 of the License, or                              -
# - (at your option) any later version.                                                            -
# -                                                                                                -
# - This program is distributed in the hope that it will be useful,                                -
# - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
# - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
# - GNU General Public License for more details.                                                   -
# -                                                                                                -
# - You should have received a copy of the GNU Affero General Public License                       -
# - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
# - #L%                                                                                            -
#---------------------------------------------------------------------------------------------------

kind: ConfigMap
apiVersion: v1
metadata:
  name: chvote-vrum-reverse-proxy-config
  labels:
    app: VRUM
    role: reverse-proxy
data:
  nginx.conf.template: |-
    worker_processes  auto;

    error_log  "/log/error.log";
    pid        "/opt/bitnami/nginx/tmp/nginx.pid";

    events {
        worker_connections  1024;
    }

    http {
        include       mime.types;
        default_type  application/octet-stream;

        add_header X-Frame-Options SAMEORIGIN;
        client_body_temp_path  "/opt/bitnami/nginx/tmp/client_body" 1 2;
        proxy_temp_path "/opt/bitnami/nginx/tmp/proxy" 1 2;
        fastcgi_temp_path "/opt/bitnami/nginx/tmp/fastcgi" 1 2;
        scgi_temp_path "/opt/bitnami/nginx/tmp/scgi" 1 2;
        uwsgi_temp_path "/opt/bitnami/nginx/tmp/uwsgi" 1 2;

        log_format main '$remote_addr - $remote_user [$time_local] '
                        '"$request" $status  $body_bytes_sent "$http_referer" '
                        '"$http_user_agent" "$http_x_forwarded_for"';

        access_log  "/log/access.log";

        # no sendfile on OSX
        sendfile        on;

        tcp_nopush     on;
        tcp_nodelay       off;

        #keepalive_timeout  0;
        keepalive_timeout  65;
        gzip on;
        gzip_http_version 1.0;
        gzip_comp_level 2;
        gzip_proxied any;
        gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

      # include "/opt/bitnami/nginx/conf/vhosts/*.conf";

      server {
        # port to listen on. Can also be set to an IP:PORT
        listen 8080 default_server;

        client_max_body_size 2000M;
        client_body_buffer_size 128k;
        client_header_buffer_size 64k;

        proxy_redirect off;
        proxy_set_header X-Real_IP $remote_addr;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_connect_timeout   90;
        proxy_send_timeout      90;
        proxy_read_timeout      90;
        proxy_buffer_size   16k;
        proxy_buffers       32   16k;
        proxy_busy_buffers_size 64k;

    ######################################################################
    # VRUM                                                               #
    ######################################################################

        resolver $DNS_SERVER valid=10s;

        location /vrum/api/ {
          set $service2 http://vrum-backend-api.$NAMESPACE.svc.cluster.local:8381; # No trailing slash
          rewrite ^/vrum/api/(.*) /api/$1 break;
          proxy_pass $service2;
        }

        location /vrum/b2b/ {
          set $service3 http://vrum-backend-b2b.$NAMESPACE.svc.cluster.local:8382; # No trailing slash
          rewrite ^/vrum/b2b/(.*) /b2b/$1 break;
          proxy_pass $service3;
        }

        location /vrum/mock/ {
          set $service4 http://vrum-mockserver-api.$NAMESPACE.svc.cluster.local:48646; # No trailing slash
          rewrite ^/vrum/mock/(.*) /mock/$1 break;
          proxy_pass $service4;
        }

        location /vrum/ {
          set $service1 http://vrum-frontend.$NAMESPACE.svc.cluster.local:8080; # No trailing slash
          rewrite ^/vrum/(.*) /$1 break;
          proxy_pass $service1;
        }

        location /status {
          stub_status on;
          access_log   off;
          allow 127.0.0.1;
          deny all;
        }
      }
    }
  mime.types: |-
    types {
        text/html                                        html htm shtml;
        text/css                                         css;
        text/xml                                         xml;
        image/gif                                        gif;
        image/jpeg                                       jpeg jpg;
        application/javascript                           js;
        application/atom+xml                             atom;
        application/rss+xml                              rss;

        text/mathml                                      mml;
        text/plain                                       txt;
        text/vnd.sun.j2me.app-descriptor                 jad;
        text/vnd.wap.wml                                 wml;
        text/x-component                                 htc;

        image/png                                        png;
        image/svg+xml                                    svg svgz;
        image/tiff                                       tif tiff;
        image/vnd.wap.wbmp                               wbmp;
        image/webp                                       webp;
        image/x-icon                                     ico;
        image/x-jng                                      jng;
        image/x-ms-bmp                                   bmp;

        application/font-woff                            woff;
        application/java-archive                         jar war ear;
        application/json                                 json;
        application/mac-binhex40                         hqx;
        application/msword                               doc;
        application/pdf                                  pdf;
        application/postscript                           ps eps ai;
        application/rtf                                  rtf;
        application/vnd.apple.mpegurl                    m3u8;
        application/vnd.google-earth.kml+xml             kml;
        application/vnd.google-earth.kmz                 kmz;
        application/vnd.ms-excel                         xls;
        application/vnd.ms-fontobject                    eot;
        application/vnd.ms-powerpoint                    ppt;
        application/vnd.oasis.opendocument.graphics      odg;
        application/vnd.oasis.opendocument.presentation  odp;
        application/vnd.oasis.opendocument.spreadsheet   ods;
        application/vnd.oasis.opendocument.text          odt;
        application/vnd.openxmlformats-officedocument.presentationml.presentation
                                                         pptx;
        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                                                         xlsx;
        application/vnd.openxmlformats-officedocument.wordprocessingml.document
                                                         docx;
        application/vnd.wap.wmlc                         wmlc;
        application/x-7z-compressed                      7z;
        application/x-cocoa                              cco;
        application/x-java-archive-diff                  jardiff;
        application/x-java-jnlp-file                     jnlp;
        application/x-makeself                           run;
        application/x-perl                               pl pm;
        application/x-pilot                              prc pdb;
        application/x-rar-compressed                     rar;
        application/x-redhat-package-manager             rpm;
        application/x-sea                                sea;
        application/x-shockwave-flash                    swf;
        application/x-stuffit                            sit;
        application/x-tcl                                tcl tk;
        application/x-x509-ca-cert                       der pem crt;
        application/x-xpinstall                          xpi;
        application/xhtml+xml                            xhtml;
        application/xspf+xml                             xspf;
        application/zip                                  zip;

        application/octet-stream                         bin exe dll;
        application/octet-stream                         deb;
        application/octet-stream                         dmg;
        application/octet-stream                         iso img;
        application/octet-stream                         msi msp msm;

        audio/midi                                       mid midi kar;
        audio/mpeg                                       mp3;
        audio/ogg                                        ogg;
        audio/x-m4a                                      m4a;
        audio/x-realaudio                                ra;

        video/3gpp                                       3gpp 3gp;
        video/mp2t                                       ts;
        video/mp4                                        mp4;
        video/mpeg                                       mpeg mpg;
        video/quicktime                                  mov;
        video/webm                                       webm;
        video/x-flv                                      flv;
        video/x-m4v                                      m4v;
        video/x-mng                                      mng;
        video/x-ms-asf                                   asx asf;
        video/x-ms-wmv                                   wmv;
        video/x-msvideo                                  avi;
    }