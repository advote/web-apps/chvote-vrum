/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api.controller

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch

import ch.ge.ve.vrum.repository.votingcard.entity.VotingChannel
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus
import ch.ge.ve.vrum.rest.api.APIEndpointTest
import ch.ge.ve.vrum.service.exception.EntityNotFoundException
import ch.ge.ve.vrum.service.exception.IllegalVotingRightStatusTransitionException
import ch.ge.ve.vrum.service.votingcard.VotingCardService
import ch.ge.ve.vrum.service.votingcard.model.CountingCircleDto
import ch.ge.ve.vrum.service.votingcard.model.DomainOfInfluenceDto
import ch.ge.ve.vrum.service.votingcard.model.VoterBirthDateDto
import ch.ge.ve.vrum.service.votingcard.model.VotingCardDto
import groovy.json.JsonSlurper
import java.time.LocalDateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

@APIEndpointTest
class VotingCardControllerTest extends Specification {

  @Autowired
  VotingCardService service

  @Autowired
  MockMvc mvc

  JsonSlurper jsonSlurper = new JsonSlurper()

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_SELECT_VOTING_CARD"])
  def "findByOperationIdAndVoterIndex should return a voting card"() {
    def operationIdStub = 1
    def voterIndexStub = 1
    def votingCard = createVotingCard(voterIndexStub, 1)

    given:
    service.findByOperationIdAndVoterIndex(1, 1) >> Optional.of(votingCard)

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards/${voterIndexStub}"))
            .andReturn().response
    def content = jsonSlurper.parseText(response.contentAsString)

    then:
    response.status == HttpStatus.OK.value()
    content.operationId == operationIdStub
    content.voterIndex == voterIndexStub
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING"])
  def "findByOperationIdAndVoterIndex should respond with a 403 status when the user does not have the right roles"() {
    when:
    def response = mvc.perform(get("/operations/1/voting-cards/1")).andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_SELECT_VOTING_CARD"])
  def "findByOperationIdAndVoterIndex should respond with a 404 status when no voting card was found"() {
    given:
    service.findByOperationIdAndVoterIndex(1, 1) >> Optional.empty()

    when:
    def response = mvc.perform(get("/operations/1/voting-cards/1")).andReturn().response

    then:
    response.status == HttpStatus.NOT_FOUND.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_SAVE_VOTING_RIGHT"])
  def "updateVotingRightStatus should save the use of a voting card's voting right"() {
    def operationIdStub = 1
    def voterIndexStub = 1
    def statusStub = VotingRightStatus.USED
    def votingChannelStub = VotingChannel.MAIL
    def urlTemplate = "/operations/${operationIdStub}/voting-cards/${voterIndexStub}" +
            "?status=${statusStub.name()}&votingChannel=${votingChannelStub.name()}"

    when:
    def response = mvc.perform(patch(urlTemplate)).andReturn().response

    then:
    response.status == HttpStatus.NO_CONTENT.value()

    and:
    1 * service.markAsUsed(operationIdStub, voterIndexStub, votingChannelStub)
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_SAVE_VOTING_RIGHT"])
  def "updateVotingRightStatus should respond with a 400 status code if the voting channel is null"() {
    def statusStub = VotingRightStatus.LOCKED
    def votingChannelStub = null
    def urlTemplate = "/operations/1/voting-cards/1" +
            "?status=${statusStub.name()}&votingChannel=${votingChannelStub}"

    given:
    service.markAsUsed(1, 1, votingChannelStub) >> { _ ->
      throw new IllegalArgumentException("Voting channel cannot be null.")
    }

    when:
    def response = mvc.perform(patch(urlTemplate)).andReturn().response

    then:
    response.status == HttpStatus.BAD_REQUEST.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_SELECT_VOTING_CARD"])
  def "updateVotingRightStatus should respond with a 403 status code when a user without the right roles tries to save a voting right usage"() {
    def statusStub = VotingRightStatus.USED
    def votingChannelStub = VotingChannel.MAIL
    def urlTemplate = "/operations/1/voting-cards/1" +
            "?status=${statusStub.name()}&votingChannel=${votingChannelStub.name()}"

    when:
    def response = mvc.perform(patch(urlTemplate)).andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_SAVE_VOTING_RIGHT"])
  def "updateVotingRightStatus should respond with a 403 status code if status is neither USED nor BLOCKED"() {
    def statusStub = VotingRightStatus.LOCKED
    def votingChannelStub = VotingChannel.MAIL
    def urlTemplate = "/operations/1/voting-cards/1" +
            "?status=${statusStub.name()}&votingChannel=${votingChannelStub.name()}"

    when:
    def response = mvc.perform(patch(urlTemplate)).andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_SAVE_VOTING_RIGHT"])
  def "updateVotingRightStatus should respond with a 404 status code if no voting card was found"() {
    def statusStub = VotingRightStatus.USED
    def votingChannelStub = VotingChannel.MAIL
    def urlTemplate = "/operations/1/voting-cards/1" +
            "?status=${statusStub.name()}&votingChannel=${votingChannelStub.name()}"

    given:
    service.markAsUsed(1, 1, votingChannelStub) >> { _ ->
      throw new EntityNotFoundException("No voting card found")
    }

    when:
    def response = mvc.perform(patch(urlTemplate)).andReturn().response

    then:
    response.status == HttpStatus.NOT_FOUND.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_BLOCK_VOTING_CARD"])
  def "updateVotingRightStatus should block a voting card"() {
    def operationIdStub = 1
    def voterIndexStub = 1
    def statusStub = VotingRightStatus.BLOCKED
    def urlTemplate = "/operations/${operationIdStub}/voting-cards/${voterIndexStub}?status=${statusStub.name()}"

    when:
    def response = mvc.perform(patch(urlTemplate)).andReturn().response

    then:
    response.status == HttpStatus.NO_CONTENT.value()

    and:
    1 * service.markAsBlocked(operationIdStub, voterIndexStub)
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_SAVE_VOTING_RIGHT"])
  def "updateVotingRightStatus should respond with a 403 status code when a user without the right roles tries to block a voting card"() {
    def statusStub = VotingRightStatus.BLOCKED
    def urlTemplate = "/operations/1/voting-cards/1?status=${statusStub.name()}"

    when:
    def response = mvc.perform(patch(urlTemplate)).andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING", "CARD_PROCESSING_SAVE_VOTING_RIGHT"])
  def "updateVotingRightStatus should respond with a 409 status code if the voting card status cannot be updated"() {
    def statusStub = VotingRightStatus.USED
    def votingChannelStub = VotingChannel.MAIL
    def urlTemplate = "/operations/1/voting-cards/1" +
            "?status=${statusStub.name()}&votingChannel=${votingChannelStub.name()}"

    given:
    service.markAsUsed(1, 1, votingChannelStub) >> { _ ->
      throw new IllegalVotingRightStatusTransitionException(createVotingCard(1, 1))
    }

    when:
    def response = mvc.perform(patch(urlTemplate)).andReturn().response

    then:
    response.status == HttpStatus.CONFLICT.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH", "SEARCH_SELECT_VOTING_CARD"])
  def "findByVoterIndex should return a voting card"() {
    def operationIdStub = 1
    def voterIndexStub = 1
    def votingCard = createVotingCard(voterIndexStub, 1)

    given:
    service.findByOperationIdAndVoterIndex(operationIdStub, voterIndexStub) >> Optional.of(votingCard)

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?voterIndex=${voterIndexStub}"))
            .andReturn().response
    def content = jsonSlurper.parseText(response.contentAsString)

    then:
    response.status == HttpStatus.OK.value()
    content[0].operationId == operationIdStub
    content[0].voterIndex == voterIndexStub
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH"])
  def "findByVoterIndex should respond with a 403 status code when a user does not have the right roles"() {
    def operationIdStub = 1
    def voterIndexStub = 1

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?voterIndex=${voterIndexStub}"))
            .andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH", "SEARCH_SELECT_VOTING_CARD"])
  def "findByVoterIndex should respond with a 404 status code when no operation corresponds to the operation ID"() {
    def operationIdStub = 1
    def voterIndexStub = 1

    given:
    service.findByOperationIdAndVoterIndex(operationIdStub, voterIndexStub) >> { _ ->
      throw new EntityNotFoundException("Operation not found")
    }

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?voterIndex=${voterIndexStub}"))
            .andReturn().response

    then:
    response.status == HttpStatus.NOT_FOUND.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH", "SEARCH_SELECT_VOTING_CARD"])
  def "findByIdentificationCode should return a voting card"() {
    def operationIdStub = 1
    def identificationCodeStub = "ID_HASH_1"
    def votingCard = createVotingCard(1, 1)

    given:
    service.findByOperationIdAndIdentificationCodeHash(
            operationIdStub,
            identificationCodeStub) >> [votingCard]

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?identificationCode=${identificationCodeStub}"))
            .andReturn().response
    def content = jsonSlurper.parseText(response.contentAsString)

    then:
    response.status == HttpStatus.OK.value()
    content[0].operationId == operationIdStub
    content[0].identificationCodeHash == identificationCodeStub
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH"])
  def "findByIdentificationCode should respond with a 403 status code when a user does not have the right roles"() {
    def operationIdStub = 1
    def identificationCodeStub = "ID_HASH_1"

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?identificationCode=${identificationCodeStub}"))
            .andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH", "SEARCH_SELECT_VOTING_CARD"])
  def "findByIdentificationCode should respond with a 404 status code when no operation corresponds to the operation ID"() {
    def operationIdStub = 1
    def identificationCodeStub = "ID_HASH_1"

    given:
    service.findByOperationIdAndIdentificationCodeHash(operationIdStub, identificationCodeStub) >> { _ ->
      throw new EntityNotFoundException("Operation not found")
    }

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?identificationCode=${identificationCodeStub}"))
            .andReturn().response

    then:
    response.status == HttpStatus.NOT_FOUND.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH", "SEARCH_SELECT_VOTING_CARD"])
  def "findByIdentificationCodeHash should return a voting card"() {
    def operationIdStub = 1
    def identificationCodeHashStub = "ID_HASH_1"
    def votingCard = createVotingCard(1, 1)

    given:
    service.findByOperationIdAndIdentificationCodeHash(
            operationIdStub,
            identificationCodeHashStub) >> [votingCard]

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?identificationCodeHash=${identificationCodeHashStub}"))
            .andReturn().response
    def content = jsonSlurper.parseText(response.contentAsString)

    then:
    response.status == HttpStatus.OK.value()
    content[0].operationId == operationIdStub
    content[0].identificationCodeHash == identificationCodeHashStub
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH"])
  def "findByIdentificationCodeHash should respond with a 403 status code when a user does not have the right roles"() {
    def operationIdStub = 1
    def identificationCodeHashStub = "ID_HASH_1"

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?identificationCodeHash=${identificationCodeHashStub}"))
            .andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH", "SEARCH_SELECT_VOTING_CARD"])
  def "findByIdentificationCodeHash should respond with a 404 status code when no operation corresponds to the operation ID"() {
    def operationIdStub = 1
    def identificationCodeHashStub = "ID_HASH_1"

    given:
    service.findByOperationIdAndIdentificationCodeHash(operationIdStub, identificationCodeHashStub) >> { _ ->
      throw new EntityNotFoundException("Operation not found")
    }

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?identificationCodeHash=${identificationCodeHashStub}"))
            .andReturn().response

    then:
    response.status == HttpStatus.NOT_FOUND.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH", "SEARCH_SELECT_VOTING_CARD"])
  def "findByLocalPersonId should return a voting card"() {
    def operationIdStub = 1
    def localPersonIdStub = "L_PERSON_ID_1"
    def votingCard = createVotingCard(1, 1)

    given:
    service.findByOperationIdAndLocalPersonId(operationIdStub, localPersonIdStub) >> [votingCard]

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?localPersonId=${localPersonIdStub}"))
            .andReturn().response
    def content = jsonSlurper.parseText(response.contentAsString)

    then:
    response.status == HttpStatus.OK.value()
    content[0].operationId == operationIdStub
    content[0].localPersonId == localPersonIdStub
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH"])
  def "findByLocalPersonId should respond with a 403 status code when a user does not have the right roles"() {
    def operationIdStub = 1
    def localPersonIdStub = "L_PERSON_ID_1"

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?localPersonId=${localPersonIdStub}"))
            .andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["ACCESS_SEARCH", "SEARCH_SELECT_VOTING_CARD"])
  def "findByLocalPersonId should respond with a 404 status code when no operation corresponds to the operation ID"() {
    def operationIdStub = 1
    def localPersonIdStub = "L_PERSON_ID_1"

    given:
    service.findByOperationIdAndLocalPersonId(operationIdStub, localPersonIdStub) >> { _ ->
      throw new EntityNotFoundException("Operation not found")
    }

    when:
    def response = mvc.perform(get("/operations/${operationIdStub}/voting-cards" +
            "?localPersonId=${localPersonIdStub}"))
            .andReturn().response

    then:
    response.status == HttpStatus.NOT_FOUND.value()
  }

  def createVotingCard(Long voterIndex, Long id, VotingRightStatus status = VotingRightStatus.AVAILABLE) {
    return new VotingCardDto(
            id,
            1,
            voterIndex,
            "ID_HASH_1",
            "L_PERSON_ID_1",
            new VoterBirthDateDto(1970, 4, 5),
            LocalDateTime.now(),
            status,
            VotingChannel.E_VOTING,
            new CountingCircleDto("6621", "Genève"),
            [new DomainOfInfluenceDto("GE", "Genève")]
    )
  }

}
