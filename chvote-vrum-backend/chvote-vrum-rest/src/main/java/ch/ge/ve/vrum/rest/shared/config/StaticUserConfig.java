/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.shared.config;

import static ch.ge.ve.vrum.rest.shared.model.ConnectedUser.BELONGS_TO_MANAGEMENT_ENTITY;
import static ch.ge.ve.vrum.rest.shared.model.ConnectedUser.BELONGS_TO_REALM;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

/**
 * Mock class for the authorized users management.
 */
@Configuration
public class StaticUserConfig {
  private static final String DEFAULT_PASS = "pass";

  private static final String CANTON_DE_GENEVE = "Canton de Genève";
  private static final String GY               = "Gy";

  private static final String USER_ROLE                                   = "ROLE_USER";
  private static final String DELETE_REGISTER_FILE                        = "ROLE_DELETE_REGISTER_FILE";
  private static final String GET_REGISTER_REPORT                         = "ROLE_GET_REGISTER_REPORT";
  private static final String UPDATE_BASE_PARAMETER                       = "ROLE_UPDATE_BASE_PARAMETER";
  private static final String UPDATE_VOTING_CARD_TITLE                    = "ROLE_UPDATE_VOTING_CARD_TITLE";
  private static final String UPLOAD_REGISTER_FILE                        = "ROLE_UPLOAD_REGISTER_FILE";
  private static final String SELECT_OPERATION                            = "ROLE_SELECT_OPERATION";
  private static final String ACCESS_CARD_PROCESSING                      = "ROLE_ACCESS_CARD_PROCESSING";
  private static final String ACCESS_SEARCH                               = "ROLE_ACCESS_SEARCH";
  private static final String ACCESS_REGISTRY_IMPORT                      = "ROLE_ACCESS_REGISTRY_IMPORT";
  private static final String SELECT_MAIL_CHANNEL                         = "ROLE_SELECT_MAIL_CHANNEL";
  private static final String SELECT_POLLING_STATION_CHANNEL              = "ROLE_SELECT_POLLING_STATION_CHANNEL";
  private static final String SELECT_OTHER_CHANNEL                        = "ROLE_SELECT_OTHER_CHANNEL";
  private static final String SELECT_BLOCKING_CHANNEL                     = "ROLE_SELECT_BLOCKING_CHANNEL";
  private static final String CARD_PROCESSING_SELECT_VOTING_CARD          = "ROLE_CARD_PROCESSING_SELECT_VOTING_CARD";
  private static final String CARD_PROCESSING_SAVE_VOTING_RIGHT           = "ROLE_CARD_PROCESSING_SAVE_VOTING_RIGHT";
  private static final String CARD_PROCESSING_BLOCKING_SELECT_VOTING_CARD = "ROLE_CARD_PROCESSING_BLOCK_VOTING_RIGHT";
  private static final String CARD_PROCESSING_BLOCK_VOTING_CARD           = "ROLE_CARD_PROCESSING_BLOCK_VOTING_CARD";
  private static final String SEARCH_SELECT_VOTING_CARD                   = "ROLE_SEARCH_SELECT_VOTING_CARD";
  private static final String SEARCH_BLOCK_VOTING_CARD                    = "ROLE_SEARCH_BLOCK_VOTING_CARD";
  private static final String SEARCH_UNBLOCK_VOTING_CARD                  = "ROLE_SEARCH_UNBLOCK_VOTING_CARD";
  private static final String SEARCH_SAVE_VOTING_RIGHT                    = "ROLE_SEARCH_SAVE_VOTING_RIGHT";

  private static final String[] ALL_ROLES = {
      USER_ROLE, DELETE_REGISTER_FILE, GET_REGISTER_REPORT, UPDATE_BASE_PARAMETER, UPDATE_VOTING_CARD_TITLE,
      UPLOAD_REGISTER_FILE, SELECT_OPERATION, ACCESS_CARD_PROCESSING, ACCESS_SEARCH, ACCESS_REGISTRY_IMPORT,
      SELECT_MAIL_CHANNEL, SELECT_POLLING_STATION_CHANNEL, SELECT_OTHER_CHANNEL, SELECT_BLOCKING_CHANNEL,
      CARD_PROCESSING_SELECT_VOTING_CARD, CARD_PROCESSING_SAVE_VOTING_RIGHT,
      CARD_PROCESSING_BLOCKING_SELECT_VOTING_CARD, CARD_PROCESSING_BLOCK_VOTING_CARD, SEARCH_SELECT_VOTING_CARD,
      SEARCH_BLOCK_VOTING_CARD, SEARCH_UNBLOCK_VOTING_CARD, SEARCH_SAVE_VOTING_RIGHT
  };

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> userConfig = auth.inMemoryAuthentication();

    addUser(userConfig, "ge1").authorities(concat(ALL_ROLES, realm("GE"), mngEnt(CANTON_DE_GENEVE)));
    addUser(userConfig, "ge2").authorities(concat(new String[]{
        USER_ROLE, SELECT_OPERATION, ACCESS_CARD_PROCESSING, SELECT_MAIL_CHANNEL, SELECT_POLLING_STATION_CHANNEL,
        CARD_PROCESSING_SELECT_VOTING_CARD, CARD_PROCESSING_SAVE_VOTING_RIGHT
    }, realm("GE"), mngEnt(CANTON_DE_GENEVE)));
    addUser(userConfig, "ge3").authorities(concat(new String[]{
        USER_ROLE, SELECT_OPERATION, ACCESS_SEARCH, SEARCH_SELECT_VOTING_CARD, SEARCH_BLOCK_VOTING_CARD,
        SEARCH_UNBLOCK_VOTING_CARD, SEARCH_SAVE_VOTING_RIGHT
    }, realm("GE"), mngEnt(CANTON_DE_GENEVE)));
    addUser(userConfig, "ge4").authorities(concat(ALL_ROLES, realm("GE"), mngEnt(GY)));
    addUser(userConfig, "ge5").authorities(realm("GE"), mngEnt(CANTON_DE_GENEVE), USER_ROLE);
  }

  private String[] concat(String[] roles, String... others) {
    String[] toReturn = Arrays.copyOf(roles, roles.length + others.length);
    System.arraycopy(others, 0, toReturn, roles.length, others.length);
    return toReturn;
  }

  private UserDetailsManagerConfigurer<AuthenticationManagerBuilder,
      InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder>>.UserDetailsBuilder addUser
      (InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> userConfig, String userName) {
    Pbkdf2PasswordEncoder passwordEncoder = new Pbkdf2PasswordEncoder();
    return userConfig.passwordEncoder(passwordEncoder)
                     .withUser(userName)
                     .password(passwordEncoder.encode(DEFAULT_PASS));
  }

  private String mngEnt(String managementEntity) {
    return BELONGS_TO_MANAGEMENT_ENTITY + managementEntity;
  }

  private String realm(String canton) {
    return BELONGS_TO_REALM + canton;
  }

}
