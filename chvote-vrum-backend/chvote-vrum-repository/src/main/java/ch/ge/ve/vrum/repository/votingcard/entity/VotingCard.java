/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.repository.votingcard.entity;

import ch.ge.ve.vrum.repository.operation.entity.Operation;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * An entity that represents a voting card.
 */
@Entity
@Table(name = "VRUM_T_VOTING_CARD",
       uniqueConstraints = @UniqueConstraint(columnNames = {"VC_N_OPERATION_ID", "VC_V_VOTER_INDEX"})
)
public class VotingCard {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "VC_N_ID")
  private Long id;

  @Column(name = "VC_V_VOTER_INDEX", nullable = false)
  private Long voterIndex;

  @Column(name = "VC_V_IDENTIFICATION_CODE_HASH", nullable = false)
  private String identificationCodeHash;

  @Column(name = "VC_V_LOCAL_PERSON_ID", nullable = false)
  private String localPersonId;

  @Column(name = "VC_N_VOTER_BIRTH_YEAR", nullable = false)
  private Integer voterBirthYear;

  @Column(name = "VC_N_VOTER_BIRTH_MONTH")
  private Integer voterBirthMonth;

  @Column(name = "VC_N_VOTER_BIRTH_DAY")
  private Integer voterBirthDay;

  @Column(name = "VC_V_VOTING_DATE_TIME")
  private LocalDateTime votingDatetime;

  @Column(name = "VC_V_VOTING_CHANNEL")
  @Enumerated(EnumType.STRING)
  private VotingChannel votingChannel;

  @Column(name = "VC_V_VOTING_RIGHT_STATUS", nullable = false)
  @Enumerated(EnumType.STRING)
  private VotingRightStatus votingRightStatus;

  @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
  @JoinColumn(name = "VC_N_OPERATION_ID", nullable = false)
  private Operation operation;

  @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
  @JoinTable(
      name = "VRUM_T_VC_DOI",
      joinColumns = {@JoinColumn(name = "VCDOI_N_VC_ID")},
      inverseJoinColumns = {@JoinColumn(name = "VCDOI_N_DOI_ID")}
  )
  private List<DomainOfInfluence> domainOfInfluences;

  @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
  @JoinColumn(name = "VC_N_COUNTING_CIRCLE_ID", nullable = false)
  private CountingCircle countingCircle;

  public Long getId() {
    return id;
  }

  public Long getVoterIndex() {
    return voterIndex;
  }

  public void setVoterIndex(Long voterIndex) {
    this.voterIndex = voterIndex;
  }

  public String getIdentificationCodeHash() {
    return identificationCodeHash;
  }

  public void setIdentificationCodeHash(String identificationCodeHash) {
    this.identificationCodeHash = identificationCodeHash;
  }

  public String getLocalPersonId() {
    return localPersonId;
  }

  public void setLocalPersonId(String localPersonId) {
    this.localPersonId = localPersonId;
  }

  public Integer getVoterBirthYear() {
    return voterBirthYear;
  }

  public void setVoterBirthYear(Integer voterBirthYear) {
    this.voterBirthYear = voterBirthYear;
  }

  public Integer getVoterBirthMonth() {
    return voterBirthMonth;
  }

  public void setVoterBirthMonth(Integer voterBirthMonth) {
    this.voterBirthMonth = voterBirthMonth;
  }

  public Integer getVoterBirthDay() {
    return voterBirthDay;
  }

  public void setVoterBirthDay(Integer voterBirthDay) {
    this.voterBirthDay = voterBirthDay;
  }

  public LocalDateTime getVotingDatetime() {
    return votingDatetime;
  }

  public void setVotingDatetime(LocalDateTime votingDatetime) {
    this.votingDatetime = votingDatetime;
  }

  public VotingChannel getVotingChannel() {
    return votingChannel;
  }

  public void setVotingChannel(VotingChannel votingChannel) {
    this.votingChannel = votingChannel;
  }

  public VotingRightStatus getVotingRightStatus() {
    return votingRightStatus;
  }

  public void setVotingRightStatus(VotingRightStatus votingRightStatus) {
    this.votingRightStatus = votingRightStatus;
  }

  public Operation getOperation() {
    return operation;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public List<DomainOfInfluence> getDomainOfInfluences() {
    return domainOfInfluences;
  }

  public void setDomainOfInfluences(List<DomainOfInfluence> domainOfInfluences) {
    this.domainOfInfluences = domainOfInfluences;
  }

  public CountingCircle getCountingCircle() {
    return countingCircle;
  }

  public void setCountingCircle(CountingCircle countingCircle) {
    this.countingCircle = countingCircle;
  }
}
