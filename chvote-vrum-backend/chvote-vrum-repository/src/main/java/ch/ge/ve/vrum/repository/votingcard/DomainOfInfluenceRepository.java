/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.repository.votingcard;

import ch.ge.ve.vrum.repository.votingcard.entity.DomainOfInfluence;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The {@link DomainOfInfluence} repository.
 */
@Repository
public interface DomainOfInfluenceRepository extends JpaRepository<DomainOfInfluence, Long> {
  /**
   * Find a {@link DomainOfInfluence} by {@link DomainOfInfluence#businessId}.
   *
   * @param businessId the {@link DomainOfInfluence#businessId}.
   *
   * @return An {@link Optional} containing the matching {@link DomainOfInfluence} if it exists.
   */
  Optional<DomainOfInfluence> findByBusinessId(String businessId);
}
