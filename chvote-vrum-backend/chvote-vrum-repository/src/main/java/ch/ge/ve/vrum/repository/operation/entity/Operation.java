/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.repository.operation.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * An entity that represents an operation.
 */
@Entity
@Table(name = "VRUM_T_OPERATION")
public class Operation {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "OP_N_ID")
  private Long id;

  @Column(name = "OP_V_PROTOCOL_ID", unique = true)
  private String protocolId;

  @Column(name = "OP_V_SHORT_LABEL", nullable = false)
  private String shortLabel;

  @Column(name = "OP_V_LONG_LABEL", nullable = false)
  private String longLabel;

  @Column(name = "OP_D_OPERATION_DATE", nullable = false)
  private LocalDateTime operationDate;

  @Column(name = "OP_V_CANTON")
  @Enumerated(EnumType.STRING)
  private Canton canton;

  @Column(name = "OP_B_TEST", nullable = false)
  private boolean test;

  public Long getId() {
    return id;
  }

  public String getProtocolId() {
    return protocolId;
  }

  public void setProtocolId(String protocolId) {
    this.protocolId = protocolId;
  }

  public String getShortLabel() {
    return shortLabel;
  }

  public void setShortLabel(String shortLabel) {
    this.shortLabel = shortLabel;
  }

  public String getLongLabel() {
    return longLabel;
  }

  public void setLongLabel(String longLabel) {
    this.longLabel = longLabel;
  }

  public LocalDateTime getOperationDate() {
    return operationDate;
  }

  public void setOperationDate(LocalDateTime operationDate) {
    this.operationDate = operationDate;
  }

  public Canton getCanton() {
    return canton;
  }

  public void setCanton(Canton canton) {
    this.canton = canton;
  }

  public boolean isTest() {
    return test;
  }

  public void setTest(boolean test) {
    this.test = test;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Operation operation = (Operation) o;
    return Objects.equals(id, operation.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
