/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.mock.server.service;

import ch.ge.ve.vrum.mock.server.utils.RandomDate;
import ch.ge.ve.vrum.repository.operation.OperationRepository;
import ch.ge.ve.vrum.repository.operation.entity.Operation;
import ch.ge.ve.vrum.repository.votingcard.CountingCircleRepository;
import ch.ge.ve.vrum.repository.votingcard.DomainOfInfluenceRepository;
import ch.ge.ve.vrum.repository.votingcard.VotingCardRepository;
import ch.ge.ve.vrum.repository.votingcard.entity.CountingCircle;
import ch.ge.ve.vrum.repository.votingcard.entity.DomainOfInfluence;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingCard;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingChannel;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service that creates and persists mocked {@link VotingCard} entities.
 */
@Service
public class VotingCardService {
  private final DomainOfInfluenceRepository domainOfInfluenceRepository;
  private final CountingCircleRepository    countingCircleRepository;
  private final OperationRepository         operationRepository;
  private final VotingCardRepository        votingCardRepository;

  private final AtomicLong votingCardCounter = new AtomicLong();

  /**
   * Create a new voting card service.
   *
   * @param domainOfInfluenceRepository the {@link DomainOfInfluenceRepository}.
   * @param countingCircleRepository    the {@link CountingCircleRepository}.
   * @param operationRepository         the {@link OperationRepository}.
   * @param votingCardRepository        the {@link VotingCardRepository}.
   */
  @Autowired
  public VotingCardService(DomainOfInfluenceRepository domainOfInfluenceRepository,
                           CountingCircleRepository countingCircleRepository,
                           OperationRepository operationRepository,
                           VotingCardRepository votingCardRepository) {
    this.domainOfInfluenceRepository = domainOfInfluenceRepository;
    this.countingCircleRepository = countingCircleRepository;
    this.operationRepository = operationRepository;
    this.votingCardRepository = votingCardRepository;
  }

  /**
   * Create and persist a set of voting cards for the given parameters.
   *
   * @param nbrOfVotingCards         the number of voting cards that will be created.
   * @param countingCircleBusinessId the business id of the {@link CountingCircle} the generated cards will be
   *                                 associated with.
   * @param doiBusinessId            the business id of the {@link DomainOfInfluence} the generated cards will be
   *                                 associated with.
   * @param protocolId       the protocl insance id of the {{@link Operation} the generated cards will be
   *                                 associated with.
   * @param votingRightStatus        the {@link VotingRightStatus} that will be assign to the generated cards.
   * @param identificationCodeHash   the identification code hash
   * @param localPersonId            the local person identifier
   * @param votingChannel            the {@link VotingChannel} that will be assign to the generated cards.
   *
   * @throws IllegalArgumentException If any of the given business ids does not match an entity already persisted in the
   *                                  database.
   */
  @Transactional
  public void createVotingCards(Long nbrOfVotingCards,
                                String countingCircleBusinessId,
                                String doiBusinessId,
                                String protocolId,
                                VotingRightStatus votingRightStatus,
                                String identificationCodeHash,
                                String localPersonId,
                                VotingChannel votingChannel) {

    Operation operation =
        operationRepository.findByProtocolId(protocolId)
                           .orElseThrow(cannotFindEntityByBusinessId(Operation.class, protocolId));

    DomainOfInfluence domainOfInfluence =
        domainOfInfluenceRepository.findByBusinessId(doiBusinessId)
                                   .orElseThrow(cannotFindEntityByBusinessId(DomainOfInfluence.class, doiBusinessId));

    CountingCircle countingCircle =
        countingCircleRepository.findByBusinessId(countingCircleBusinessId)
                                .orElseThrow(
                                    cannotFindEntityByBusinessId(CountingCircle.class, countingCircleBusinessId));

    LocalDate voterBirthDate = getRandomBirthDate();

    for (int i = 0; i < nbrOfVotingCards; i++) {
      Long voterIndex = votingCardCounter.getAndIncrement();
      String voterIdentificationCodeHash = identificationCodeHash;
      String voterLocalPersonId = localPersonId;

      if (identificationCodeHash == null) {
        voterIdentificationCodeHash = String.format("%04d", voterIndex);
      }

      if (localPersonId == null) {
        voterLocalPersonId = String.format("%04d", voterIndex);
      }

      VotingCard votingCard = new VotingCard();

      votingCard.setVoterIndex(voterIndex);
      votingCard.setIdentificationCodeHash(voterIdentificationCodeHash);
      votingCard.setLocalPersonId(voterLocalPersonId);

      if (identificationCodeHash == null && localPersonId == null) {
        voterBirthDate = getRandomBirthDate();
      }

      votingCard.setVoterBirthYear(voterBirthDate.getYear());
      votingCard.setVoterBirthMonth(voterBirthDate.getMonthValue());
      votingCard.setVoterBirthDay(voterBirthDate.getDayOfMonth());

      votingCard.setOperation(operation);
      votingCard.setDomainOfInfluences(Collections.singletonList(domainOfInfluence));
      votingCard.setCountingCircle(countingCircle);

      votingCard.setVotingChannel(votingChannel);
      votingCard.setVotingRightStatus(votingRightStatus);

      if (VotingRightStatus.USED.equals(votingRightStatus)) {
        votingCard.setVotingDatetime(getRandomVotingDate());
      }

      votingCardRepository.save(votingCard);
    }
  }

  /**
   * Delete all the persisted entities regardless they were created by this service.
   */
  @Transactional
  public void deleteAll() {
    votingCardRepository.deleteAll();
    votingCardCounter.set(0);
  }

  private LocalDate getRandomBirthDate() {
    return RandomDate.get(
        LocalDate.now().minusYears(80),
        LocalDate.now().minusYears(18)
    );
  }

  private LocalDateTime getRandomVotingDate() {
    return RandomDate.get(
        LocalDateTime.now().minusDays(1),
        LocalDateTime.now()
    );
  }

  private Supplier<IllegalArgumentException> cannotFindEntityByBusinessId(Class<?> entityType, String businessId) {
    return () -> new IllegalArgumentException(
        String.format("Cannot retrieve [%s] by business id [%s]", entityType.getSimpleName(), businessId)
    );
  }
}
