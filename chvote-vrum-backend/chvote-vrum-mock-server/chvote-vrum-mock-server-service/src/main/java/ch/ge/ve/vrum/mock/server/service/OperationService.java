/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.mock.server.service;

import ch.ge.ve.vrum.repository.operation.OperationRepository;
import ch.ge.ve.vrum.repository.operation.entity.Canton;
import ch.ge.ve.vrum.repository.operation.entity.Operation;
import com.google.common.collect.ImmutableMap;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service that creates and persists mocked {@link Operation} entities.
 */
@Service
public class OperationService {
  private static final DateTimeFormatter   SHORT_LABEL_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd'VP'");
  private static final String              LONG_LABEL_FORMAT       = "Operation %s";
  private static final Map<String, String> OPERATIONS              = ImmutableMap.of(
      "OP_1", "20200922VP",
      "OP_2", "20211215VP",
      "OP_3", "20190306VP",
      "OP_4", "20221021VP",
      "OP_5", "21000101VP"
  );
  private static final List<String>        OPERATIONS_IN_TEST      = List.of("OP_5");

  private final OperationRepository operationRepository;

  /**
   * Create a new operation service.
   *
   * @param operationRepository the {@link OperationRepository}.
   */
  @Autowired
  public OperationService(OperationRepository operationRepository) {
    this.operationRepository = operationRepository;
  }

  /**
   * Create and persist a set of predefined {@link Operation} entities:
   *
   * <h3 id="predefined">Predefined Operations</h3>
   * <table class="striped" style="text-align:left">
   * <thead>
   * <tr>
   * <th scope="col">protocolId</th>
   * <th scope="col">shortLabel</th>
   * <th scope="col">test</th>
   * </tr>
   * </thead>
   * <tbody>
   * <tr>
   * <th scope="row">OP_1</th>
   * <td>20200922VP</td>
   * <td>false</td>
   * </tr>
   * <tr>
   * <th scope="row">OP_2</th>
   * <td>20211215VP</td>
   * <td>false</td>
   * </tr>
   * <tr>
   * <th scope="row">OP_3</th>
   * <td>20190306VP</td>
   * <td>false</td>
   * </tr>
   * <tr>
   * <th scope="row">OP_4</th>
   * <td>20221021VP</td>
   * <td>false</td>
   * </tr>
   * <tr>
   * <th scope="row">OP_5</th>
   * <td>21000101VP</td>
   * <td>true</td>
   * </tr>
   * </tbody>
   * </table>
   */
  @Transactional
  public void createAll() {
    OPERATIONS.forEach((protocolId, shortLabel) -> {
      Operation operation = new Operation();

      operation.setProtocolId(protocolId);
      operation.setShortLabel(shortLabel);
      operation.setLongLabel(String.format(LONG_LABEL_FORMAT, shortLabel));
      operation.setOperationDate(LocalDate.parse(shortLabel, SHORT_LABEL_DATE_FORMAT).atStartOfDay());
      operation.setCanton(Canton.GE);
      operation.setTest(OPERATIONS_IN_TEST.contains(protocolId));

      operationRepository.save(operation);
    });
  }

  /**
   * Delete all the persisted entities regardless that they were created by this service.
   */
  @Transactional
  public void deleteAll() {
    operationRepository.deleteAll();
  }
}
