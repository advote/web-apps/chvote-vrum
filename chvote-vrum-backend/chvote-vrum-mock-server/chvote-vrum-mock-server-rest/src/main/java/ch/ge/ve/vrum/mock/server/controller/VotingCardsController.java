/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.mock.server.controller;

import ch.ge.ve.vrum.mock.server.service.VotingCardService;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingChannel;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A REST controller that enables the creation of voting cards entities.
 */
@RestController
@RequestMapping("/voting-cards")
@Api(
    value = "Voting cards",
    tags = "Voting cards")
public class VotingCardsController {

  private final VotingCardService votingCardService;

  /**
   * Create a new voting cards controller.
   *
   * @param votingCardService the {@link VotingCardService}.
   */
  @Autowired
  public VotingCardsController(VotingCardService votingCardService) {
    this.votingCardService = votingCardService;
  }

  /**
   * Create and persists a set of voting cards for the given parameters.
   *
   * @param nbrOfVotingCards         the number of voting cards that will be created.
   * @param countingCircleBusinessId the business id of the counting circle the generated cards will be associated
   *                                 with.
   * @param doiBusinessId            the business id of the domain of influence the generated cards will be associated
   *                                 with.
   * @param protocolId               the protocol id of the operation the generated cards will be associated with.
   * @param votingRightStatus        the {@link VotingRightStatus} that will be assigned to the generated cards.
   * @param identificationCodeHash   the identification code hash that will be assigned to the generated cards.
   * @param localPersonId            the local person identifier that will be assigned to the generated cards.
   * @param votingChannel            the {@link VotingChannel} that will be assigned to the generated cards.
   */
  @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  @ApiOperation(
      value = "Create voting cards",
      notes = "Create a set of voting cards with the given parameters.\n\n" +
              "The voter index in the generated cards starts from 0 " +
              "and it increments with each generated card as such:\n" +
              "* A first call generates 10 cards for the protocol id 'OP_1'. " +
              "Their voter index will range from 0 to 9.\n" +
              "* A second call generated 2 cards for the protocol id 'OP_2'. " +
              "Their voter index will be 10 and 11 respectively.\n\n" +
              "If the database is emptied this number will be reset.\n\n" +
              "If the identification code hash or the local person ID is set, the cards will " +
              "be created for the same voter, with the same date of birth.")
  @ApiImplicitParams(
      {
          @ApiImplicitParam(paramType = "form", required = true, value = "The number of voting cards to generate.",
                            name = "nbrOfVotingCards"),
          @ApiImplicitParam(paramType = "form", required = true, value = "The counting circle id.", example = "6621",
                            name = "countingCircleBusinessId"),
          @ApiImplicitParam(paramType = "form", required = true, value = "The domain of influence circle id.",
                            name = "doiBusinessId"),
          @ApiImplicitParam(paramType = "form", required = true, value = "The protocol id.", example = "OP_1",
                            name = "protocolId"),
          @ApiImplicitParam(paramType = "form", required = true, value = "The voting right status.",
                            name = "votingRightStatus"),
          @ApiImplicitParam(paramType = "form", value = "The identification code hash.",
                            name = "identificationCodeHash"),
          @ApiImplicitParam(paramType = "form", value = "The local person ID.", name = "localPersonId"),
          @ApiImplicitParam(paramType = "form", value = "The voting channel.", name = "votingChannel")
      }
  )
  public void createVotingCards(Long nbrOfVotingCards,
                                String countingCircleBusinessId,
                                String doiBusinessId,
                                String protocolId,
                                VotingRightStatus votingRightStatus,
                                String identificationCodeHash,
                                String localPersonId,
                                VotingChannel votingChannel) {
    votingCardService.createVotingCards(
        nbrOfVotingCards,
        countingCircleBusinessId,
        doiBusinessId,
        protocolId,
        votingRightStatus,
        identificationCodeHash,
        localPersonId,
        votingChannel
    );
  }
}
