/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.operation;

import ch.ge.ve.vrum.repository.operation.OperationRepository;
import ch.ge.ve.vrum.repository.operation.entity.Operation;
import ch.ge.ve.vrum.service.exception.EntityNotFoundException;
import ch.ge.ve.vrum.service.mapper.BeanMapper;
import ch.ge.ve.vrum.service.operation.model.OperationDto;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service managing {@link Operation} entities.
 */
@Service
public class OperationService {

  private final OperationRepository operationRepository;

  /**
   * Creates a new operation service.
   *
   * @param operationRepository the {@link OperationRepository}.
   */
  @Autowired
  public OperationService(OperationRepository operationRepository) {
    this.operationRepository = operationRepository;
  }

  /**
   * Get all ongoing operations.
   *
   * @return all the ongoing operations.
   */
  @Transactional(readOnly = true)
  public List<OperationDto> findAll() {
    return operationRepository.findAll()
                              .stream()
                              .map(BeanMapper::map)
                              .collect(Collectors.toList());
  }

  /**
   * Retrieve a list of all ongoing operations matching the given test property.
   *
   * @param test whether the returned operations should be flagged as test operations.
   *
   * @return all the matching operations.
   */
  @Transactional(readOnly = true)
  public List<OperationDto> findByTest(boolean test) {
    return operationRepository.findByTest(test)
                              .map(BeanMapper::map)
                              .collect(Collectors.toList());
  }

  /**
   * Retrieve the operation corresponding to the given id.
   *
   * @param id the operation id.
   *
   * @return the operation matching the given id.
   *
   * @throws EntityNotFoundException if there were no entities matching the given parameters.
   */
  @Transactional(readOnly = true)
  public OperationDto findById(Long id) {
    return operationRepository
        .findById(id)
        .map(BeanMapper::map)
        .orElseThrow(() -> new EntityNotFoundException(String.format(
            "Cannot find any operation with id [%s]", id)));
  }

}
