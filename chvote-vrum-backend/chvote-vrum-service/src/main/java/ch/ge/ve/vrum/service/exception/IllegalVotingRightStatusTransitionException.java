/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.exception;

import ch.ge.ve.vrum.service.votingcard.model.VotingCardDto;

/**
 * Thrown to indicate that a requested update of the voting right status is incompatible with the current status.
 */
public class IllegalVotingRightStatusTransitionException extends IllegalStateException {

  private final transient VotingCardDto votingCard;

  /**
   * Create a new illegal voting right status transition exception. The given parameters are used to create the
   * exception's message.
   *
   * @param votingCard the voting card for which the status is in conflict with a requested status update.
   *
   * @see RuntimeException#RuntimeException(String)
   */
  public IllegalVotingRightStatusTransitionException(VotingCardDto votingCard) {
    super(String.format("The current status [%s] doesn't allow for a status update" +
                        " on voting card with voter index [%d] and operation ID [%s].",
                        votingCard.getVotingRightStatus(),
                        votingCard.getVoterIndex(),
                        votingCard.getOperationId()));

    this.votingCard = votingCard;
  }

  public VotingCardDto getVotingCard() {
    return this.votingCard;
  }

}
